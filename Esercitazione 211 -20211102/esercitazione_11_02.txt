-----------------------------------------------------------------------------
CORREZIONE ESERCIZIO PER CASA
-----------------------------------------------------------------------------

1) Si consideri il linguaggio L delle stringhe su alfabeto {a,b,c} per cui 
non vale che il numero delle "a", delle "b" e delle "c" � lo stesso (vi sono
almeno due tra "a", "b" e "c" per cui la quantit� delle une � diversa dalla
quantit� delle altre). Dire se il linguaggio L �, o meno, libero dal contesto
e dimostrare la propria affermazione. Qual'� il liguaggio complementare di L?

2) Provare a sostituire, in SimpleExp.g4 e in SimpleCalcSTVisitor.java, 
l'operatore "+" con l'operatore "-" (la grammatica ottenuta � stata chiamata 
SimpleExpMinus.g4) e confrontare il risultato ottenuto impostando 
l'associativita' a destra con quella a sinistra.  
ESEMPIO: 3-(4-2-7)*5

-----------------------------------------------------------------------------
REALIZZAZIONE DELLA STACK VIRTUAL MACHINE (SVM) E DEL RELATIVO ASSEMBLATORE
-----------------------------------------------------------------------------

Realizziamo una Stack Virtual Machine (SVM) e il relativo assemblatore.
Esempio: compilazione ed esecuzione file sorgente quicksort.fool

             compilazione                       assembly
quicksort.fool --------> quicksort.fool.asm    ----------> codice oggetto 
                         (assembler della SVM)                    |
                                                                  |
                                                                  V
                                                           Stack Virtual Machine

1) Creiamo un progetto FOOL in cui iniziare la creazione del nostro compilatore
per il linguaggio Functional Object Oriented Language (FOOL).
All'interno creiamo un package "svm" in cui collocare i files iniziali forniti
nella directory "svm".

2) Creiamo assemblatore SVM in SVM.g4 ed esecutore di codice oggetto SVM in 
ExecuteVM.java. TestASM.java consente di assemblare ed eseguire un file ".asm"

-----------------------------------------------------------------------------
ESPRESSIONI CON MULTIPLI OPERATORI A STESSO LIVELLO DI PRIORITA' (Exp.g4)
-----------------------------------------------------------------------------

L'utilizzo di grammatiche EBNF, invece di semplici CFG, consente di estendere 
la grammatica SimpleExp.g4 aggiungendo a "+" e "*" anche le operazioni "-" e 
"/" (intero della divisione tra interi), con 
- operazioni "*" e "/" allo stesso livello di priorit� (pi� alto)
- operazioni "+" e "-" allo stesso livello di priorit� (pi� basso)
- per entrambi i livelli di priorit� associativit� a sinistra

1) Costruiamo la specifica lessicale/sintattica Exp.g4 
- aggiungendo i token DIV e MINUS a quelli di SimpleExpDec.g4 
- modificando la prima e la seconda produzione di SimpleExp.g4 in modo che 
generino, rispettivamente: l'operazione "*" o "/", e l'operazione "+" o "-".

2) Testiamo l'espressione (15-7+2)*5/10 sia utilizzando la associativit� a 
sinistra per entrambe le produzioni, sia utilizzando la associativit� a 
destra.

-----------------------------------------------------------------------------
CALCOLO RISULTATO PER ESPRESSIONI CON MULTIPLI OPERATORI A STESSA PRIORITA' -----------------------------------------------------------------------------

Nei nodi degli alberi sintattici della grammatica Exp.g4, es. per un nodo di 
classe "ExpProd1Context", si possono avere due casi: o i suoi figli sono i 
nodi di una espressione "exp" "*" "exp", o di una espressione "exp" "/" "exp".

1) Costruiamo una classe visitor "CalcSTVisitor.java" modificando il codice 
di visita in modo da considerare tali due casi. 
ESEMPIO: (15-7+2)*5/10

2) Aggiungiamo la possibilit� di gestire, a livello di sintassi, numeri 
negativi oltre che positivi ed estendiamo il visitor di conseguenza.
ESEMPIO: (15-7+2)*-5/10

-----------------------------------------------------------------------------
ESERCIZIO PER CASA: 
COMPLETARE IMPLEMENTAZIONE VIRTUAL MACHINE PER GRAMMATICA IN SVM.g4
-----------------------------------------------------------------------------

Completare l'implementazione della Virtual Machine considerando l'intera 
grammatica SVM.g4 (terminando tutte le operazioni).

Sono previsti i seguenti registri interni
HP (heap pointer)
FP (frame pointer)
RA (return address)
TM (temporary storage)
da implementare come campi analogmente ai registri IP e SP.

Inizialmente il registro FP deve avere lo stesso valore di SP mentre HP 
deve essere inizializzato a 0.

Come test, provare il file "quicksort.fool.asm" che deve stampare:
1
2
2
3
4
5

