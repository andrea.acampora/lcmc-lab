package exp;
import java.io.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

public class Test {
    public static void main(String[] args) throws Exception {

        String fileName = "prova.txt";
     
        CharStream chars = CharStreams.fromFileName(fileName);
        SimpleExpLexer lexer = new SimpleExpLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SimpleExpParser parser = new SimpleExpParser(tokens);
        ParseTree prog = parser.prog();
        System.out.println("Lexical errors: "+ lexer.lexicalErrors);
        System.out.println("Syntex errors: "+ parser.getNumberOfSyntaxErrors());
        SimpleCalcSTVisitor simpleCalcSTVisitor = new SimpleCalcSTVisitor();
        System.out.println(simpleCalcSTVisitor.visit(prog));
    }
}
