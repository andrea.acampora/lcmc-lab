package exp;
import java.io.*;
import org.antlr.v4.runtime.*;

//lexicalErrors
//getNumberOfSyntaxErrors

public class Test {
    public static void main(String[] args) throws Exception {

        String fileName = "prova.txt";
     
        CharStream chars = CharStreams.fromFileName(fileName);
        SimpleExpLexer lexer = new SimpleExpLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SimpleExpParser parser = new SimpleExpParser(tokens);
        
        }
}
